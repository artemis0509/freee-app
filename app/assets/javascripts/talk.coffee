# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

App.talk = App.cable.subscriptions.create "TalkChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    $('#messages').append data['message']

  speak: (message) ->
    @perform 'speak', message: message
    
$(document).on 'keypress', '[data-behavior~=talk_speaker]', (event) ->
  if event.keyCode is 13 # return = send
    App.talk.speak event.target.value
    event.target.value = ''
    event.preventDefault()
